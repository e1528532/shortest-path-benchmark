package at.ac.tuwien.fmi2016;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.ALTAdmissibleHeuristic;
import org.jgrapht.alg.shortestpath.AStarShortestPath;
import org.jgrapht.alg.shortestpath.BellmanFordShortestPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.alg.shortestpath.FloydWarshallShortestPaths;
import org.jgrapht.graph.DefaultEdge;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.profile.GCProfiler;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

/**
 * Used to measure the memory footprints of the algorithms by analyzing the GC activity.
 */
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class ShortestPathMemoryBenchmark {

	@State(Scope.Thread)
	public static class RandomState extends BenchmarkState {

		public RandomState() {
			super(ShortestPathScenarioGenerator::generateRandomScenario);
		}

	}

	@State(Scope.Thread)
	public static class LinearState extends BenchmarkState {

		public LinearState() {
			super(ShortestPathScenarioGenerator::generateLinearScenario);
		}

	}

	@State(Scope.Thread)
	public static class RingState extends BenchmarkState {

		public RingState() {
			super(ShortestPathScenarioGenerator::generateRingScenario);
		}

	}

	@State(Scope.Thread)
	public static class StarState extends BenchmarkState {

		public StarState() {
			super(ShortestPathScenarioGenerator::generateStarScenario);
		}

	}

	public static void main(final String[] args) throws RunnerException {
		final String vertices = "50";
		final Options opt = new OptionsBuilder() //
				.include(".*" + ShortestPathMemoryBenchmark.class.getSimpleName() + ".*") //
				.param("vertices", vertices) // Adjust these to the size you wanna try
				.warmupIterations(2) //
				.measurementIterations(5) //
				.shouldDoGC(true) //
				.addProfiler(GCProfiler.class) //
				.result("measurement-memory-" + vertices + ".csv") //
				.resultFormat(ResultFormatType.CSV) //
				.forks(1).build();

		new Runner(opt).run();
	}

	@Benchmark
	public void dijkstraRandom(final RandomState scenario, final Blackhole blackhole) {
		template(scenario, blackhole, DijkstraShortestPath<Integer, DefaultEdge>::new);
	}

	@Benchmark
	public void floydWarshallRandom(final RandomState scenario, final Blackhole blackhole) {
		template(scenario, blackhole, FloydWarshallShortestPaths<Integer, DefaultEdge>::new);
	}

	@Benchmark
	public void aStarRandom(final RandomState scenario, final Blackhole blackhole) {
		template(scenario, blackhole, graph -> new AStarShortestPath<>(graph,
				new ALTAdmissibleHeuristic<>(scenario.getScenario().getGraph(), scenario.getScenario().getLandmarks())));
	}

	@Benchmark
	public void bellmanFordRandom(final RandomState scenario, final Blackhole blackhole) {
		template(scenario, blackhole, BellmanFordShortestPath<Integer, DefaultEdge>::new);
	}

	@Benchmark
	public void dijkstraLinear(final LinearState scenario, final Blackhole blackhole) {
		template(scenario, blackhole, DijkstraShortestPath<Integer, DefaultEdge>::new);
	}

	@Benchmark
	public void floydWarshallLinear(final LinearState scenario, final Blackhole blackhole) {
		template(scenario, blackhole, FloydWarshallShortestPaths<Integer, DefaultEdge>::new);
	}

	@Benchmark
	public void aStarLinear(final LinearState scenario, final Blackhole blackhole) {
		template(scenario, blackhole, graph -> new AStarShortestPath<>(graph,
				new ALTAdmissibleHeuristic<>(scenario.getScenario().getGraph(), scenario.getScenario().getLandmarks())));
	}

	@Benchmark
	public void bellmanFordLinear(final LinearState scenario, final Blackhole blackhole) {
		template(scenario, blackhole, BellmanFordShortestPath<Integer, DefaultEdge>::new);
	}

	@Benchmark
	public void dijkstraRing(final RingState scenario, final Blackhole blackhole) {
		template(scenario, blackhole, DijkstraShortestPath<Integer, DefaultEdge>::new);
	}

	@Benchmark
	public void floydWarshallRing(final RingState scenario, final Blackhole blackhole) {
		template(scenario, blackhole, FloydWarshallShortestPaths<Integer, DefaultEdge>::new);
	}

	@Benchmark
	public void aStarRing(final RingState scenario, final Blackhole blackhole) {
		template(scenario, blackhole, graph -> new AStarShortestPath<>(graph,
				new ALTAdmissibleHeuristic<>(scenario.getScenario().getGraph(), scenario.getScenario().getLandmarks())));
	}

	@Benchmark
	public void bellmanFordRing(final RingState scenario, final Blackhole blackhole) {
		template(scenario, blackhole, BellmanFordShortestPath<Integer, DefaultEdge>::new);
	}

	@Benchmark
	public void dijkstraStar(final StarState scenario, final Blackhole blackhole) {
		template(scenario, blackhole, DijkstraShortestPath<Integer, DefaultEdge>::new);
	}

	@Benchmark
	public void floydWarshallStar(final StarState scenario, final Blackhole blackhole) {
		template(scenario, blackhole, FloydWarshallShortestPaths<Integer, DefaultEdge>::new);
	}

	@Benchmark
	public void aStarStar(final StarState scenario, final Blackhole blackhole) {
		template(scenario, blackhole, graph -> new AStarShortestPath<>(graph,
				new ALTAdmissibleHeuristic<>(scenario.getScenario().getGraph(), scenario.getScenario().getLandmarks())));
	}

	@Benchmark
	public void bellmanFordStar(final StarState scenario, final Blackhole blackhole) {
		template(scenario, blackhole, BellmanFordShortestPath<Integer, DefaultEdge>::new);
	}

	private void template(final StateScenario stateScenario, final Blackhole blackhole,
			final Function<Graph<Integer, DefaultEdge>, ShortestPathAlgorithm<Integer, DefaultEdge>> algorithm) {
		final ShortestPathScenario scenario = stateScenario.getScenario();
		final GraphPath<Integer, DefaultEdge> path = algorithm.apply(scenario.getGraph()).getPath(scenario.getStart(), scenario.getEnd());
		blackhole.consume(path);
	}

}
