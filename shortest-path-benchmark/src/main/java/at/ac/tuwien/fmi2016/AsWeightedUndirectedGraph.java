package at.ac.tuwien.fmi2016;

import java.util.Map;

import org.jgrapht.Graph;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.AsWeightedGraph;

public class AsWeightedUndirectedGraph<V, E> extends AsWeightedGraph<V, E> implements UndirectedGraph<V, E> {

	private static final long serialVersionUID = 6751689424260266550L;

	public AsWeightedUndirectedGraph(final Graph<V, E> g, final Map<E, Double> weightMap) {
		super(g, weightMap);
	}

}
