package at.ac.tuwien.fmi2016;

import java.util.Set;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;

/**
 * Contains a scenario for our shortest path benchmark consisting of a graph with a start and a end goal randomly chosen. For the K-Star, it
 * also chooses a set of random landmarks to use the heuristics in a way.
 */
public class ShortestPathScenario {

	private final Graph<Integer, DefaultEdge> graph;

	private final int start;

	private final int end;

	private final Set<Integer> landmarks;

	public ShortestPathScenario(final Graph<Integer, DefaultEdge> graph, final int start, final int end, final Set<Integer> landmarks) {
		super();
		this.graph = graph;
		this.start = start;
		this.end = end;
		this.landmarks = landmarks;
	}

	public Graph<Integer, DefaultEdge> getGraph() {
		return graph;
	}

	public int getStart() {
		return start;
	}

	public int getEnd() {
		return end;
	}

	public Set<Integer> getLandmarks() {
		return landmarks;
	}

}
