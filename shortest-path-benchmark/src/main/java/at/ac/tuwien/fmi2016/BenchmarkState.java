package at.ac.tuwien.fmi2016;

import java.util.function.Function;

import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

@State(Scope.Benchmark)
public abstract class BenchmarkState implements StateScenario {

	@Param(value = "100")
	private int vertices;
	private final Function<Integer, ShortestPathScenario> generator;
	private ShortestPathScenario scenario;

	public BenchmarkState(final Function<Integer, ShortestPathScenario> generator) {
		this.generator = generator;
	}

	@Setup(Level.Iteration)
	public void prepare() {
		System.out.println("Generating new " + getClass().getSimpleName() + " scenario");
		scenario = generator.apply(vertices);
		System.out.println("Finished generating new " + getClass().getSimpleName() + " scenario with "
				+ scenario.getGraph().vertexSet().size() + " vertices and " + scenario.getGraph().edgeSet().size() + " edges");
	}

	@Override
	public ShortestPathScenario getScenario() {
		return scenario;
	}

}