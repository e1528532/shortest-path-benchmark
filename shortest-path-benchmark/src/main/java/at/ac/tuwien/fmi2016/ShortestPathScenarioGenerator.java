package at.ac.tuwien.fmi2016;

import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.jgrapht.Graph;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.VertexFactory;
import org.jgrapht.generate.CompleteBipartiteGraphGenerator;
import org.jgrapht.generate.GnmRandomGraphGenerator;
import org.jgrapht.generate.GraphGenerator;
import org.jgrapht.generate.LinearGraphGenerator;
import org.jgrapht.generate.RingGraphGenerator;
import org.jgrapht.generate.StarGraphGenerator;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.Pseudograph;

/**
 * Utility class to generate different scenarios for measurement.
 */
public class ShortestPathScenarioGenerator {

	private ShortestPathScenarioGenerator() {
		// Utility class
	}

	public static ShortestPathScenario generateRandomScenario(final int vertices) {
		return generateScenario(new GnmRandomGraphGenerator<>(vertices, vertices, System.currentTimeMillis()));
	}

	public static ShortestPathScenario generateStarScenario(final int vertices) {
		return generateScenario(new StarGraphGenerator<>(vertices));
	}

	public static ShortestPathScenario generateRingScenario(final int vertices) {
		return generateScenario(new RingGraphGenerator<>(vertices));
	}

	public static ShortestPathScenario generateLinearScenario(final int vertices) {
		return generateScenario(new LinearGraphGenerator<>(vertices));
	}

	public static ShortestPathScenario generateCompleteBipartiteGraphScenario(final int vertices) {
		return generateScenario(new CompleteBipartiteGraphGenerator<>(vertices / 2, vertices / 2));
	}

	private static ShortestPathScenario generateScenario(final GraphGenerator<Integer, DefaultEdge, Integer> gen) {
		final UndirectedGraph<Integer, DefaultEdge> graph = new Pseudograph<>(DefaultEdge.class);
		final IntegerVertexFactory factory = new IntegerVertexFactory();
		gen.generateGraph(graph, factory, null);

		final int start = factory.getRandom(graph);
		int end = factory.getRandom(graph);
		while (start == end) {
			// that would be rather boring otherwise
			end = factory.getRandom(graph);
		}

		return new ShortestPathScenario(new AsWeightedUndirectedGraph<>(graph, new WeightRandomizer().randomizeWeight(graph)), start, end,
				factory.getLandmarks(graph));
	}

	private static class WeightRandomizer {

		private final Random random = new Random();
		private static final int MAX_WEIGHT = 100;
		private static final int MIN_WEIGHT = 0;

		public Map<DefaultEdge, Double> randomizeWeight(final Graph<Integer, DefaultEdge> graph) {
			return graph.edgeSet().stream()
					.collect(Collectors.toMap(Function.identity(), e -> (double) MIN_WEIGHT + random.nextInt(MAX_WEIGHT - MIN_WEIGHT)));
		}

	}

	private static class IntegerVertexFactory implements VertexFactory<Integer> {

		private int id;

		private final Random random = new Random();

		@Override
		public Integer createVertex() {
			return ++id;
		}

		public Integer getRandom(final Graph<Integer, DefaultEdge> graph) {
			int v = random.nextInt(id);
			while (!graph.containsVertex(v)) {
				v = random.nextInt(id);
			}
			return v;
		}

		public Set<Integer> getLandmarks(final Graph<Integer, DefaultEdge> graph) {
			final int modulo = (int) Math.sqrt(graph.vertexSet().size());
			return graph.vertexSet().stream().filter(i -> i.intValue() % modulo == 0).collect(Collectors.toSet());
		}

	}

}
