package at.ac.tuwien.fmi2016;

public interface StateScenario {

	ShortestPathScenario getScenario();

}