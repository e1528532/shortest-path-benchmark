# Installation

Its a maven project which can be imported e.g. in Eclipse. Since i made use of the latest unreleased version of the library, which contains various improvements for the shortest path algorithms, it is necessary to clone the repository https://github.com/d-michail/jgrapht and build it via maven to get the latest version on your computer.

After that, the benchmarks can be run. Depending on your IDE its necessary to configure the annotation post processor, which generate the actual benchmark code. The required processor is included via the maven dependency jmh-generator-annprocess, it depends on the IDE how to add that.